/* main.c
I need this file to start preparing some structure in my head. Alex
*/

#define MAXTARGETS 2048
#define BUFFER_LENGTH 4 // we do something very simple and studpid here
#define NUM_CAMS 4

#include "main.h"

tracking_run* run;
Calibration* calib[NUM_CAMS];
control_par* cpar;
coord_2d** corrected;
n_tupel* corresp_buf;


// These functions are part of the a test suite, see under /tests

void read_all_calibration(Calibration* calib[NUM_CAMS], control_par* cpar)
{
#ifdef _WIN32
	char ori_tmpl[] = "calibration\\cam%d.tif.ori";
	char added_name[] = "calibration\\cam%d.tif.addpar";
#else
	char ori_tmpl[] = "calibration/cam%d.tif.ori";
	char added_name[] = "calibration/cam%d.tif.addpar";
#endif
	char ori_name[40];
	char added_name2[40];
	int cam;
	for (cam = 0; cam < cpar->num_cams; cam++)
	{
		//sprintf(ori_name, ori_tmpl, cam + 1);
		sprintf(ori_name, ori_tmpl, cam);
		//sprintf(added_name2, added_name, cam + 1);
		sprintf(added_name2, added_name, cam);
		//calib[cam] = read_calibration(ori_name, added_name, NULL);
		calib[cam] = read_calibration(ori_name, added_name2, NULL);
	}
}


/*  correct_frame() performs the transition from pixel to metric to flat
	coordinates and x-sorting as required by the correspondence code.

	Arguments:
	frame *frm - target information for all cameras.
	control_par *cpar - parameters of image size, pixel size etc.
	tol - tolerance parameter for iterative flattening phase, see
		trafo.h:correct_brown_affine_exact().
*/
coord_2d** correct_frame(frame* frm, Calibration* calib[], control_par* cpar, double tol)
{
	coord_2d** corrected, ** tmp;
	int cam;
	int part;

	corrected = (coord_2d**)malloc(cpar->num_cams * sizeof(coord_2d*));
	tmp = (coord_2d**)malloc(cpar->num_cams * sizeof(coord_2d*));
	for (cam = 0; cam < cpar->num_cams; cam++)
	{
		corrected[cam] = (coord_2d*)malloc(frm->num_targets[cam] * sizeof(coord_2d));
		tmp[cam] = (coord_2d*)malloc(frm->num_targets[cam] * sizeof(coord_2d));
		if (corrected[cam] == NULL)
		{
			/* roll back allocations and fail */
			for (cam -= 1; cam >= 0; cam--)
			{
				free(corrected[cam]);
			}
			free(corrected);
			return NULL;
		}

		for (part = 0; part < frm->num_targets[cam]; part++)
		{
			pixel_to_metric(&tmp[cam][part].x, &tmp[cam][part].y,
				frm->targets[cam][part].x, frm->targets[cam][part].y,
				cpar);

			dist_to_flat(tmp[cam][part].x, tmp[cam][part].y,
				calib[cam], &corrected[cam][part].x, &corrected[cam][part].y,
				tol);
			corrected[cam][part].pnr = frm->targets[cam][part].pnr;
		}

		/* This is expected by find_candidate() */
		quicksort_coord2d_x(corrected[cam], frm->num_targets[cam]);
	}
	return corrected;
}


void init(char* params_directory_path, char* ptv_file_name, char* sequence_file_name, char* track_file_name, char* criteria_file_name,
	int frame_first, int frame_last)
{
	chdir(params_directory_path);

	cpar = read_control_par(ptv_file_name);
	read_all_calibration(calib, cpar);
	free_control_par(cpar);

	run = tr_new_legacy(sequence_file_name,
		track_file_name, criteria_file_name,
		ptv_file_name, calib);

	run->seq_par->first = frame_first;
	run->seq_par->last = frame_last;
}


void compute_3d_paths(target*** input_targets, int** num_targets, frame** output_3d_paths)
{
	int i;
	int cam_index;
	int step;
	int frame_index;
	int match_counts[4];
	corres t_corres = { 3, {96, 66, 26, 26} };
	vec2d targ[4];
	vec3d res;
	P t_path = {
		.x = {45.219, -20.269, 25.946},
		.prev = -1,
		.next = -2,
		.prio = 4,
		.finaldecis = 1000000.0,
		.inlist = 0.
	};

	// for each camera and for each time step the images are processed
	for (step = run->seq_par->first; step < run->seq_par->last + 1; step++)
	{
		frame_index = step - run->seq_par->first; // local step

		for (cam_index = 0; cam_index < run->cpar->num_cams; cam_index++)
		{

			// read targets
			read_input_targets(run->fb->buf, input_targets, num_targets, frame_index, cam_index);
			// end read targets

			quicksort_target_y(run->fb->buf[frame_index]->targets[cam_index], run->fb->buf[frame_index]->num_targets[cam_index]);

			for (i = 0; i < run->fb->buf[frame_index]->num_targets[cam_index]; i++)
			{
				run->fb->buf[frame_index]->targets[cam_index][i].pnr = i;
			}

		} // inner loop is per camera

		corrected = correct_frame(run->fb->buf[frame_index], run->cal, run->cpar, 0.00001);
		corresp_buf = correspondences(run->fb->buf[frame_index], corrected, run->vpar, run->cpar, run->cal, match_counts);

		run->fb->buf[frame_index]->num_parts = match_counts[0];

		// shortcut
		int p[4];
		double x, y;

		for (i = 0; i < run->fb->buf[frame_index]->num_parts; i++)
		{
			for (cam_index = 0; cam_index < run->cpar->num_cams; cam_index++)
			{
				if (corresp_buf[i].p[cam_index] > -1)
				{
					p[cam_index] = corrected[cam_index][corresp_buf[i].p[cam_index]].pnr;
					x = run->fb->buf[frame_index]->targets[cam_index][p[cam_index]].x;
					y = run->fb->buf[frame_index]->targets[cam_index][p[cam_index]].y;

					pixel_to_metric(&x, &y, x, y, run->cpar);
					dist_to_flat(x, y, run->cal[cam_index], &x, &y, 0.00001);

					targ[cam_index][0] = x;
					targ[cam_index][1] = y;
				}
				else
				{
					targ[cam_index][0] = 1e-10;
					targ[cam_index][1] = 1e-10;
				}

				t_corres.p[cam_index] = p[cam_index];
			}

			t_corres.nr = i + 1; // identical to what I see in rt_is.10004 file
			run->fb->buf[frame_index]->correspond[i] = t_corres;

			point_position(targ, run->cpar->num_cams, run->cpar->mm, run->cal, res);

			t_path.x[0] = res[0];
			t_path.x[1] = res[1];
			t_path.x[2] = res[2];

			run->fb->buf[frame_index]->path_info[i] = t_path;
		}

	} // external loop is through frames

	run->tpar->add = 0;

	trackcorr_c_loop(run, run->seq_par->first);



	output_3d_paths = run->fb->buf;
}


void dispose()
{
	int cam_index = run->fb->num_cams;

	for (cam_index -= 1; cam_index >= 0; cam_index--)
	{
		free(corrected[cam_index]);
	}
	free_calibration(calib, run->fb->num_cams);
	free(corrected);
	free(corresp_buf);
	free(run->vpar);
	free_control_par(run->cpar);
	tr_free(run);
}


// int main(int argc, const char *argv[])
int main()
{
	// initialize variables

	int i, cam_index, step, frame_index;
	coord_2d** corrected = NULL;
	int match_counts[4];
	n_tupel* corresp_buf = NULL;
	tracking_run* run;
	vec3d res;
	vec2d targ[4];
	//framebuf* frm;

	// temporary variables will be replaced per particle 
	corres t_corres = { 3, {96, 66, 26, 26} };
	P t_path = {
		.x = {45.219, -20.269, 25.946},
		.prev = -1,
		.next = -2,
		.prio = 4,
		.finaldecis = 1000000.0,
		.inlist = 0.
	};

	// read parameters from the working directory
	// for simplicity all names are default and hard coded (sorry)

	// 1. process inputs: directory, first frame, last frame

	// printf("This program was called with \"%s\".\n", argv[0]);

	// argc = 4;

	// argv[1] = '../tests/testing_fodder/test_cavity/';
	// argv[2] = 10001;
	// argv[3] = 10004;

	// if (argc != 2 && argc != 4)
	// {
	//     printf("Wrong number of inputs, expecting: \n");
	//     printf(" ./main ../tests/testing_fodder/test_cavity/ 10001 10004 \n");
	//     return 0;
	// }

	// argv[1] = '../tests/testing_fodder/test_cavity/';
	// argv[2] = 10001;
	// argv[3] = 10004;

	// change directory to the user-supplied working folder
	// chdir(argv[1]);

	// chdir("../tests/testing_fodder/test_cavity/");
	//chdir("/Users/alex/Documents/OpenPTV/test_cavity");
	//int ret =_chdir("E:\\tomer\\TAU\\3DVisualization\\3D_visualization\\OpenPTV\\openptv-dfc7b1f08d5212e9159e84686ece4bfdde947ddd\\liboptv\\tests\\testing_fodder\\test_cavity");
	//int ret = _chdir("C:\\Users\\Tomer\\source\\repos\\openptv-refactor_fb\\liboptv\\tests\\testing_fodder\\test_cavity");

	Calibration* calib[4]; // sorry only for 4 cameras now

#ifdef _WIN32

	int ret = _chdir("..\\tests\\1Vision_test\\1Vision_sequence");
	
	control_par* cpar = read_control_par("parameters\\ptv.par");
	read_all_calibration(calib, cpar);
	free_control_par(cpar);

	run = tr_new_legacy("parameters\\sequence.par",
		"parameters\\track.par", "parameters\\criteria.par",
		"parameters\\ptv.par", calib);
#else
	// CHANGE DIRECTORY FOR YOUR NEEDS
	chdir("./tests/1Vision_test/1Vision_sequence");

	control_par* cpar = read_control_par("parameters/ptv.par");
	read_all_calibration(calib, cpar);
	free_control_par(cpar);

	run = tr_new_legacy("parameters/sequence.par",
		"parameters/track.par", "parameters/criteria.par",
		"parameters/ptv.par", calib);
#endif

	// printf("changed directory to %s\n", argv[1]);

	// 2. read parameters and calibrations

	run->seq_par->first = 10000001;
	run->seq_par->last = 10000004;

	// for each camera and for each time step the images are processed
	for (step = run->seq_par->first; step < run->seq_par->last + 1; step++)
	{
		frame_index = step - run->seq_par->first; // local step

		for (cam_index = 0; cam_index < run->cpar->num_cams; cam_index++)
		{
			// we decided to focus camust on the _targets, so we will read them from the
			// test directory test_cavity

			// read frames code was here before...
			run->fb->buf[frame_index]->num_targets[cam_index] = read_targets(run->fb->buf[frame_index]->targets[cam_index], run->fb->target_file_base[cam_index], step);
			// end read frames 

			quicksort_target_y(run->fb->buf[frame_index]->targets[cam_index], run->fb->buf[frame_index]->num_targets[cam_index]);

			for (i = 0; i < run->fb->buf[frame_index]->num_targets[cam_index]; i++)
			{
				run->fb->buf[frame_index]->targets[cam_index][i].pnr = i;
			}

		} // inner loop is per camera

		corrected = correct_frame(run->fb->buf[frame_index], run->cal, run->cpar, 0.00001);

		corresp_buf = correspondences(run->fb->buf[frame_index], corrected, run->vpar, run->cpar, run->cal, match_counts);

		run->fb->buf[frame_index]->num_parts = match_counts[0];

		// first we need to create 3d points after correspondences and fill it into the buffer
		// use point_position and loop through the num_parts
		// probably feed it directly into the buffer

		// so we split into two parts:
		// first i copy the code from correspondences.pyx
		// and create the same types of arrays in C
		// then we will convert those to 3D using similar approach to what is now in Python

		// shortcut
		int p[4];
		double x, y;
		// float skew_dist;

		for (i = 0; i < run->fb->buf[frame_index]->num_parts; i++)
		{
			for (cam_index = 0; cam_index < run->cpar->num_cams; cam_index++)
			{
				if (corresp_buf[i].p[cam_index] > -1)
				{
					p[cam_index] = corrected[cam_index][corresp_buf[i].p[cam_index]].pnr;
					x = run->fb->buf[frame_index]->targets[cam_index][p[cam_index]].x;
					y = run->fb->buf[frame_index]->targets[cam_index][p[cam_index]].y;

					pixel_to_metric(&x, &y, x, y, run->cpar);
					dist_to_flat(x, y, run->cal[cam_index], &x, &y, 0.00001);

					targ[cam_index][0] = x;
					targ[cam_index][1] = y;
				}
				else
				{
					targ[cam_index][0] = 1e-10;
					targ[cam_index][1] = 1e-10;
				}

				t_corres.p[cam_index] = p[cam_index];
			}

			t_corres.nr = i + 1; // identical to what I see in rt_is.10004 file
			run->fb->buf[frame_index]->correspond[i] = t_corres;

			point_position(targ, run->cpar->num_cams, run->cpar->mm, run->cal, res);

			t_path.x[0] = res[0];
			t_path.x[1] = res[1];
			t_path.x[2] = res[2];

			run->fb->buf[frame_index]->path_info[i] = t_path;
		}

	} // external loop is through frames

	run->tpar->add = 0;
	// we do not need to read frames - it's all in memory now
	// track_forward_start(run); 

	trackcorr_c_loop(run, run->seq_par->first);

	// probably here we need to send something to plot it
	// or store in some memory for the next chunk?
	// basically we have a human in the loop here - his/her brain
	// will simply follow the velocity values in time like a movie
	// and we will store it to binary files. Later if someone wants to do
	// tracking, our simmple solution is not good enough. we kind of doing 3D-PIV here
	// of 4 frames and show the vectors. The quasi-vectors are not really connected. if we
	// will create nice animation - then the user will build tracamectories him/herself.


	// warite paths results to file
	for (frame_index = 0; frame_index < run->fb->buf_len; frame_index++)
	{
		char currFileName[45];
		char* format = "actualTestResults\\Paths_frame%d.txt";
		sprintf(currFileName, format, frame_index + 1);
		FILE* file = fopen(currFileName, "w+");
		if (file)
		{
			for (i = 0; i < run->fb->buf[frame_index]->num_parts; i++)
			{
				fprintf(file, "%f %f %f %d %d\n",
					run->fb->buf[frame_index]->path_info[i].x[0],//x
					run->fb->buf[frame_index]->path_info[i].x[1],//y
					run->fb->buf[frame_index]->path_info[i].x[2],//z
					run->fb->buf[frame_index]->path_info[i].next,
					run->fb->buf[frame_index]->path_info[i].prev);
			}
			fclose(file);
		}
	}


	// free resources
	for (cam_index -= 1; cam_index >= 0; cam_index--)
	{
		free(corrected[cam_index]);
	}
	free(corrected);
	free(corresp_buf);
	free(run->vpar);
	free_control_par(run->cpar);

	return 0;

} // should be end of main now
