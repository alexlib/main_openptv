#ifndef main_h

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
// #include <direct.h>// instead of <dirent.h> 
#include <math.h>
#include <string.h>
#include <inttypes.h>
//#include "tiffio.h"

#include "parameters.h"
#include "track.h"
#include "image_processing.h"
#include "segmentation.h"
#include "tracking_frame_buf.h"
#include "calibration.h"
#include "vec_utils.h"
#include "imgcoord.h"
#include "trafo.h"
#include "correspondences.h"



#define main_h


#endif /* main_h */